#include "stdafx.h"
#include <cstdio>
#include <iostream>
#include <string>
#include <list>
#include <cstdlib>
using namespace std;

template<typename pizza>
class Node {
public:
	int rozmiar;
	int position;
	int size;
	pizza information;
	Node *vater;
	Node **sohne;
	Node<pizza> get_vater();
	bool is_vater();
	void set_vater(Node<pizza> *vater);
	pizza get_info();
	void set_info(pizza information);
	void add_sohn(Node<pizza> *sohn);
	//void dodaj_wezel(pizza dane, Node<pizza>* root);
	//	void wyswietl();
	void przesun();
	//class Node<pizza> *temp;
	//class Node<pizza> *sohne;
	//public:
	Node();
	//	Node(Node<pizza> *vater);
	Node(Node<pizza> *vater, pizza information);
};

template<typename pizza>
Node<pizza>::Node() {
	//sohne = new Node<pizza>[lop];
	sohne = new Node*[4];
	sohne[0] = NULL;
	sohne[1] = NULL;
	sohne[2] = NULL;
	sohne[3] = NULL;
	size = 4;
	rozmiar = 0;
	position = 0;
	vater = NULL;
	//sohne = new Node<pizza>[rozmiar];
	//temp = NULL;
	//cout << "Node" << endl;
	//cout << "Node" << endl;
}

template<typename pizza>
Node<pizza>::Node(Node<pizza> *ojciec, pizza info)
{
	//sohne = new Node<pizza>[lop];
	sohne = new Node*[4];
	sohne[0] = NULL;
	sohne[1] = NULL;
	sohne[2] = NULL;
	sohne[3] = NULL;
	size = 4;
	rozmiar = 0;
	position = 0;
	vater = ojciec;
	information = info;
}

template<typename pizza>
Node<pizza> Node<pizza>::get_vater() {
	return *vater;
}

template<typename pizza>
bool Node<pizza>::is_vater() {
	//cout << "isvater" << endl;
	if (vater == NULL)
	{
		return false;
	}
	else {
		return true;
	}
}

template<typename pizza>
void Node<pizza>::set_vater(Node<pizza> *ojciec) {
	vater = ojciec;
}

template<typename pizza>
void Node<pizza>::przesun() {
	position = position + 1;
}

template<typename pizza>
pizza Node<pizza>::get_info() {
	return information;
}

template<typename pizza>
void Node<pizza>::set_info(pizza info) {
	information = info;
}

template<typename pizza>
void Node<pizza>::add_sohn(Node<pizza> *cos) {

	//cout << this->information << "  ";
	int rozmiar = this->rozmiar;
	if (rozmiar < size - 2)
	{
		//int rozmiar = this->rozmiar;
		//cout << "wchodze" << endl;
		this->sohne[rozmiar] = cos;
		this->rozmiar = this->rozmiar + 1;
		rozmiar = rozmiar + 1;
		//cout << "ROZMIAR : " << this->rozmiar << endl;
	}
	else
	{
		//cout << "SIZE:" << size << endl;
		int prt = size * 2;
		Node <pizza> **tab = new Node<pizza>*[prt];
		for (int i = 0; i < rozmiar; i++)
		{
			tab[i] = this->sohne[i];
		}
		this->sohne = tab;
		this->sohne[rozmiar] = cos;
		this->size = size * 2;
		this->rozmiar = this->rozmiar + 1;
		rozmiar = this->rozmiar;
		for (int i = rozmiar; i < size; i++)
		{
			this->sohne[i] = nullptr;
		}
	}
}


template<typename pizza>
class Baum {
private:
	Node<pizza> *root;
public:
	void Pre_order(Node<pizza> *wezel);
	void Post_order(Node<pizza> *wezel);
	void In_order(Node<pizza> *wezel);
	int korzen();
	void usun(pizza usuwam, Node<pizza>* wezel);
	int wysokosc(int licznik, Node<pizza> *wezel);
	void dodaj_wezel(Node<pizza> *temp, pizza dane);
	//Node(Node<pizza> *vater, pizza information);
	Baum();
	Baum(Node<pizza> *root);
};

template<typename pizza>
Baum<pizza>::Baum() {
	root = NULL;
}

template<typename pizza>
int Baum<pizza>::korzen() {
	//cout << root->information << endl;
	return root->rozmiar;
}

template<typename pizza>
int Baum<pizza>::wysokosc(int licznik, Node<pizza>* wezel)
{
	int h = licznik;
	int	rozmiar = wezel->rozmiar;
	//cout << wezel->information << endl;
	Node<pizza> *temp = new Node<pizza>();
	for (int i = 0; i < rozmiar; i++)
	{
		temp = wezel->sohne[i];
		h++;
		wysokosc(h, temp);
	}
	//cout << h << endl;
	return h;
}

template<typename pizza>
Baum<pizza>::Baum(Node<pizza> *nowy)
{
	//cout << nowy->rozmiar << endl;
	root = nowy;
	//cout << root->rozmiar << endl;
}

template<typename pizza>
void Baum<pizza>::Pre_order(Node<pizza> *wezel)
{
	int	rozmiar = wezel->rozmiar;
	cout << wezel->information << " ";
	Node<pizza> *temp = new Node<pizza>();
	for (int i = 0; i < rozmiar; i++)
	{
		temp = wezel->sohne[i];
		Pre_order(temp);
	}
}

template<typename pizza>
void Baum<pizza>::Post_order(Node<pizza> *wezel)
{
	//cout << wezel->rozmiar << endl;
	int	rozmiar = wezel->rozmiar;
	Node<pizza> *temp = new Node<pizza>();
	for (int i = 0; i < rozmiar; i++)
	{
		temp = wezel->sohne[i];
		Post_order(temp);
	}
	cout << wezel->information << " ";
}

template<typename pizza>
void Baum<pizza>::In_order(Node<pizza> *wezel)
{
	//cout << wezel->rozmiar << endl;
	if (wezel->sohne[0] != nullptr)
		In_order(wezel->sohne[0]);
	cout << wezel->information << " ";
	for (int i = 1; i<wezel->rozmiar; i++)
		In_order(wezel->sohne[i]);
}

template<typename pizza>
void Baum<pizza>::usun(pizza usuwam, Node<pizza> *wezel)
{
	//int	rozmiar = wezel->rozmiar;
	Node<pizza> *temp = new Node<pizza>();
	for (int i = 0; i < wezel->rozmiar; i++)
	{
		if (wezel->sohne[i]->information != usuwam)
		{
			temp = wezel->sohne[i];
			usun(usuwam, temp);
		}
		else
		{
			wezel->sohne[i] = nullptr;
			Node<pizza> **pomoc = new Node<pizza>*[wezel->size];
			int j = 0;
			for (int i = 0; i < wezel->size; i++)
			{
				if (j < wezel->size) {
					if (wezel->sohne[j] != nullptr)
					{
						pomoc[i] = wezel->sohne[j];
						j++;
					}
					else
					{
						j++;
						pomoc[i] = wezel->sohne[j];
						j++;
					}
				}
			}
			wezel->sohne = pomoc;
			wezel->rozmiar = wezel->rozmiar - 1;
			for (int i = wezel->rozmiar; i < wezel->size; i++)
			{
				wezel->sohne[i] = nullptr;
			}
		}
	}
}

template<typename pizza>
void Baum<pizza>::dodaj_wezel(Node<pizza>* temp, pizza dane)
{
	if (root == nullptr)
	{
		//cout << "costam" << endl;
		Node<pizza> *n1 = new Node<pizza>(nullptr, dane);
		root = n1;
	}
	else
	{
		//cout << "costam2" << endl;
		//Node<pizza> *temp = new Node<pizza>();
		//temp = root;
		int trp = temp->rozmiar;
		trp = trp + 1;
		int zmienna = rand() % trp;
		if (temp->sohne[zmienna] == nullptr)
		{
			Node<pizza> *n1 = new Node<pizza>(temp, dane);
			temp->add_sohn(n1);
		}
		else
		{
			temp = temp->sohne[zmienna];
			dodaj_wezel(temp, dane);
			//cos = temp->rozmiar;
			//cos = cos + 1;1
			//zmienna = rand() % cos;
		}
	}
}

int main()
{
	Node<int> *wezel = new Node<int>(NULL, 5);
	//Node<string> *temp = new Node<string>();
	int dane;
	//dane = "cos 2";
	Baum<int> *drzewo = new Baum<int>(wezel);

	//cout << rand()% cos << endl;
	//cout << wezel->rozmiar;
	dane = 1;
	//cout << wezel->rozmiar;
	drzewo->dodaj_wezel(wezel, dane);
	dane = 2;
	//cout << wezel->rozmiar;
	drzewo->dodaj_wezel(wezel, dane);
	dane = 3;
	//cout << wezel->rozmiar;
	drzewo->dodaj_wezel(wezel, dane);
	dane = 4;
	//cout << wezel->rozmiar;
	drzewo->dodaj_wezel(wezel, dane);
	dane = 5;
	//cout << wezel->rozmiar;
	drzewo->dodaj_wezel(wezel, dane);
	dane = 6;
	//cout << wezel->rozmiar;
	drzewo->dodaj_wezel(wezel, dane);
	cout << "Post" << endl;
	cout << "Wysokosc: ";
	cout << drzewo->wysokosc(0, wezel) << endl;
	drzewo->Post_order(wezel);
	cout << endl;
	cout << "Pre" << endl;
	drzewo->Pre_order(wezel);
	cout << endl;
	cout << "In" << endl;
	drzewo->In_order(wezel);
	cout << endl;
	drzewo->usun(4, wezel);
	//drzewo->usun(3, wezel);
	cout << "Post" << endl;
	cout << "Wysokosc: ";
	cout << drzewo->wysokosc(0, wezel) << endl;
	drzewo->Post_order(wezel);
	cout << endl;
	cout << "Pre" << endl;
	drzewo->Pre_order(wezel);
	cout << endl;
	cout << "In" << endl;
	drzewo->In_order(wezel);
	cout << endl;
	return 0;
}
